const express = require("express");
const mongoose = require("mongoose");
const bodyParser = require("body-parser");
const cors = require("cors");
const { ApolloServer } = require("apollo-server-express");

const schema = require("./schema");
const { getPayload } = require("./utils");

const URL = "mongodb://localhost:27017/customerdb";

const connect = mongoose.connect(URL, { useNewUrlParser: true });
connect.then(
  db => {
    console.log("Connected successfully to server!");
  },
  err => {
    console.log(err);
  }
);

const server = new ApolloServer({
  schema: schema,
  context: ({ req }) => {
    const token = req.headers.authorization || "";
    console.log("got token from request", req.headers)
    const { payload: user, loggedIn } = getPayload(token);

    return { user, loggedIn };
  }
});

const app = express();

app.use(bodyParser.json());
app.use("*", cors());

server.applyMiddleware({ app });

app.listen({ port: 3001 }, () =>
  console.log(`🚀 Server ready at http://localhost:3001${server.graphqlPath}`)
);
