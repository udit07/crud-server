const { gql, makeExecutableSchema } = require("apollo-server-express");

const Customer = require("./models/customer").Customers;
const User = require("./models/user").User;
const { getToken, encryptPassword, comparePassword } = require("./utils");

const typeDefs = gql`
  type User {
    id: ID
    email: String!
    username: String!
    password: String!
    token: String
  }
  type Customer {
    id: ID!
    first_name: String!
    last_name: String!
    address: String!
  }
  type Query {
    getCustomers: [Customer]
    getCustomer(id: ID!): Customer
  }
  type Mutation {
    register(email: String!, username: String!, password: String!): User
    login(username: String!, password: String!): User
    addCustomer(
      first_name: String!
      last_name: String!
      address: String!
    ): Customer
    updateCustomer(
      id: ID!
      first_name: String!
      last_name: String!
      address: String!
    ): Customer
    deleteCustomer(id: ID!): Customer
  }
`;

const resolvers = {
  Query: {
    getCustomers: (parent, args, context) => {
      console.log("get all customers", context)
      if (!context.loggedIn) throw new AuthenticationError("Unauthorized request!");
      return Customer.find({});
    },
    getCustomer: (parent, args, context) => {
      if (!context.loggedIn) throw new AuthenticationError("Unauthorized request!");
      return Customer.findById(args.id);
    }
  },
  Mutation: {
    register: async (parent, args, context, info) => {
      const newUser = new User({
        email: args.email,
        username: args.username,
        password: await encryptPassword(args.password)
      });
      const user = await User.findOne({ username: args.username });
      if (user) {
        throw new AuthenticationError("User Already Exists!");
      }

      return newUser.save();
    },
    login: async (parent, args, context, info) => {
      const user = await User.findOne({ username: args.username });
      const isMatch = await comparePassword(args.password, user.password);
      if (isMatch) {
        const payload = {
          user: {
            id: user.id
          }
        };
        const token = getToken(payload);
        const loginUser = new User({
          id: user.id,
          username: user.username,
          email: user.email,
          token
        });
        console.log("login user request", token);
        return loginUser;
      } else {
        throw new AuthenticationError("Wrong Password!");
      }
    },
    addCustomer: (parent, args, context) => {
      if (!context.loggedIn) throw new AuthenticationError("Unauthorized request!");
      let newCustomer = new Customer({
        first_name: args.first_name,
        last_name: args.last_name,
        address: args.address
      });
      return newCustomer.save();
    },
    updateCustomer: (parent, args, context) => {
      if (!context.loggedIn) throw new AuthenticationError("Unauthorized request!");
      if (!args.id) return;
      return Customer.findOneAndUpdate(
        {
          _id: args.id
        },
        {
          $set: {
            first_name: args.first_name,
            last_name: args.last_name,
            address: args.address
          }
        },
        { new: true },
        (err, Customer) => {
          if (err) {
            console.log("Something went wrong when updating the customer");
          } else {
            return Customer;
          }
        }
      );
    },
    deleteCustomer: (parent, args, context) => {
      if (!context.loggedIn)
        throw new AuthenticationError("Unauthorized request!");
      if (!args.id) return;
      return Customer.findByIdAndRemove(args.id);
    }
  }
};

module.exports = makeExecutableSchema({
  typeDefs: [typeDefs],
  resolvers: resolvers
});
